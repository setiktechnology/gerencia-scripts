#!/bin/bash
#################################################################
# block-user.sh - Paulo Cesar Garcia
# Gera arquivo htaccess restrito por senha
# bash <(GET https://bitbucket.org/setiktechnology/gerencia-scripts/raw/d5a718524c08f26557e19bcc72613e6784dfb524/block-user-htaccess.sh)
# 
# Please submit all bug reports at paulo@setik.com.py
#
#################################################################


: ${AGORA:=`date '+%Y%m%d%H%M%S'`}
: ${U_USERNAME:=`pwd | cut -d'/' -f3`}
: ${U_PASSWORD:=`openssl rand -base64 10`}

if [ `pwd | cut -d'/' -f2` != "home" ]
then
	echo 'Você deve estar dentro do home do usuario para executar este comando'
	exit
fi

htpasswd -b -c /home/$U_USERNAME/.htpasswd $U_USERNAME $U_PASSWORD
chown $U_USERNAME:$U_USERNAME /home/$U_USERNAME/.htpasswd

echo "
AuthName 'Acceso Denegado, Contacte con el Soporte.'
AuthType Basic
AuthUserFile /home/$U_USERNAME/.htpasswd
AuthGroupFile /dev/null
require valid-user
" >> /home/$U_USERNAME/.htaccess
chown $U_USERNAME:$U_USERNAME /home/$U_USERNAME/.htaccess

ALLOW_PASSWORD_CHANGE=1  /scripts/chpass $U_USERNAME $U_PASSWORD

echo "User: $U_USERNAME"
echo "Password: $U_PASSWORD"
